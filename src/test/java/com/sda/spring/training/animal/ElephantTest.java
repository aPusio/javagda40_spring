package com.sda.spring.training.animal;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class ElephantTest {
	@Mock
	private ElephantEmotionComponent emotionComponent;

	@Mock
	private ElephantSoundComponent soundComponent;

	@InjectMocks
	private Elephant elephant;

	@Test
	public void elephantShouldReturnPfffWhenHeIsWesoly(){
		//given


		//when
		Mockito.when(emotionComponent.getEmotion(ElephantState.HAPPY))
			.thenReturn("emotka");

		Mockito.when(soundComponent.getSound(ElephantState.HAPPY))
			.thenReturn("dzwiek");

		String wesolyDzwiek = elephant.sound(ElephantState.HAPPY);

		//then
		assertEquals("emotka dzwiek", wesolyDzwiek);
	}

	@Test
	public void hmm(){
		//given

		//when
		String wesolyDzwiek = elephant.sound(null);

		//then
		assertEquals("NULL value is not supported", wesolyDzwiek);
	}
}