package com.sda.spring.training;

import java.time.LocalDate;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class DataObject {
	private String name;
	private LocalDate date;
}
