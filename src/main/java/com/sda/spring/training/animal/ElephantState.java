package com.sda.spring.training.animal;

public enum ElephantState {
	HAPPY,
	ANGRY
}
