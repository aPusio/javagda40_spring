package com.sda.spring.training.animal;

import org.springframework.stereotype.Component;

@Component
public class ElephantSoundComponent {

	public String getSound(ElephantState mood){
		String returnedValue = null;
		switch(mood){
			case HAPPY:
				returnedValue = "PFFFFFFFFFFFFFFFFFFF";
				break;
			case ANGRY:
				returnedValue = "pffff";
				break;
			default:
				returnedValue = null;
		}
		return returnedValue;
	}
}
