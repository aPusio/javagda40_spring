package com.sda.spring.training.animal;

import java.util.Objects;

import org.slf4j.Logger;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;

@Component
@Primary
public class Elephant implements Animal{
	private static final String ELEPHANT_SOUND = "PFFFFF";
	private static final Logger log = org.slf4j.LoggerFactory.getLogger(Elephant.class);

	private ElephantEmotionComponent elephantEmotionComponent;
	private ElephantSoundComponent elephantSoundComponent;

	public Elephant(ElephantEmotionComponent elephantEmotionComponent, ElephantSoundComponent elephantSoundComponent) {
		this.elephantEmotionComponent = elephantEmotionComponent;
		this.elephantSoundComponent = elephantSoundComponent;
	}

	@Override
	public void sound(){
//		System.out.println("ELEPHANT: " + ELEPHANT_SOUND);
		log.info("ELEPHANT: " + ELEPHANT_SOUND);
	}

	public String sound(ElephantState mood){
		if(mood == null ){
			return "NULL value is not supported";
		}
//		String returnedValue = null;
//		if(mood != null && mood.equals("wesoly")){
//		if("wesoly".equals(mood)){
//		if(Objects.equals(mood, "wesoly")) {
//			returnedValue = "PFFFFFFFFFFFFFFFFFFF";
//		}
//		if(Objects.equals(mood, "smutny")){
//			returnedValue  = ":( pffff";
//		}
		String returnedValue =
			elephantEmotionComponent.getEmotion(mood)
			+ " "
			+ elephantSoundComponent.getSound(mood);

		return returnedValue;
	}
}
