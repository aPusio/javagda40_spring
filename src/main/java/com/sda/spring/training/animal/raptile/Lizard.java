package com.sda.spring.training.animal.raptile;

import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
@Order(1)
public class Lizard implements Reptile {
	@Override
	public void sound() {
		log.info("LIZARD: SY SY");
	}
}
