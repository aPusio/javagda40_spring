package com.sda.spring.training.animal;

import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
public class Dog implements Animal {
	private static final String DOG_SOUND = "wof wof";

	@Override
	public void sound() {
		log.info("DOG: {}", DOG_SOUND);
	}
}
