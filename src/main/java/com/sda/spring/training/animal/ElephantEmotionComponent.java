package com.sda.spring.training.animal;

import org.springframework.stereotype.Component;

@Component
public class ElephantEmotionComponent {

	public String getEmotion(ElephantState mood){
		String returnedValue = null;
		switch (mood){
			case ANGRY:
				returnedValue =  ":(";
				break;
			case HAPPY:
				returnedValue =  ":)";
				break;
		}
		return returnedValue;
	}
}
