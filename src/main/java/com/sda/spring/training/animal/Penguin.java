package com.sda.spring.training.animal;

import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
public class Penguin implements Animal {
	private static final String PENGUIN_SOUND = "SRU TU TUUU TU TU";

	@Override
	public void sound() {
		log.info("PENGUIN: {}", PENGUIN_SOUND);
	}
}
