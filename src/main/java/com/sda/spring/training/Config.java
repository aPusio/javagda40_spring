package com.sda.spring.training;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.LocalDate;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class Config {

	@Bean
	public BufferedReader getBufferedReader(){
		BufferedReader bufferedReader = null;
		try {
			bufferedReader = Files.newBufferedReader(Path.of("C:\\SDA\\training\\src\\main\\resources\\welcome.txt"));
		} catch (IOException e) {
			e.printStackTrace();
		}
		return bufferedReader;
	}

	@Bean
	public DataObject getDataObject(){
		DataObject myObject =
			new DataObject("my object", LocalDate.now());
		return myObject;
	}
}
